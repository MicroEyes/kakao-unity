﻿using UnityEngine;
using System.Collections;

public class KakaoManager : MonoBehaviour {

    private static KakaoManager s_instance = null;

    [SerializeField]
    private bool m_isDebug = true;
    public static bool IsDebug
    {
        get { return s_instance.m_isDebug; }
    }

    void Awake()
    {
        s_instance = this;
    }
    void OnGUI()
    {
        float Y = 0.0f;
        if (GUI.Button(new Rect(10.0f, Y, Screen.width * 0.4f, Screen.height * 0.2f), "INITIALIZE", "button"))
        {
            CS_Java_Bridge.Initialize();
        }

        Y += Screen.height * 0.2f;

        if (GUI.Button(new Rect(10.0f, Y, Screen.width * 0.4f, Screen.height * 0.2f), "SEND TEXT", "button"))
        {
            CS_Java_Bridge.SendText("Google Me");
        }

        Y += Screen.height * 0.2f;

        if (GUI.Button(new Rect(10.0f, Y, Screen.width * 0.4f, Screen.height * 0.2f), "SEND APP LINK", "button"))
        {
            CS_Java_Bridge.SendAppLink("AppLink_Text");
        }

        Y += Screen.height * 0.2f;


        if (GUI.Button(new Rect(10.0f, Y, Screen.width * 0.4f, Screen.height * 0.2f), "SEND WEB LINK", "button"))
        {
            CS_Java_Bridge.SendWebLink("WebLink_Text", "web_link");
        } 
    }

	// Use this for initialization
	void Start () {
        	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
