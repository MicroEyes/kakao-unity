﻿using UnityEngine;
using System.Collections;

public class CS_Java_Bridge {

    static CS_Java_Bridge s_instance = null;
    static CS_Java_Bridge Instance
    {
        get
        {
            if (s_instance == null)
            {
                s_instance = new CS_Java_Bridge();
            }
            return s_instance;
        }
    }

    static AndroidJavaClass admobPluginClass;
    static AndroidJavaClass unityPlayer;
    static AndroidJavaObject currActivity;

    //Preventing new from Outside Class
    private CS_Java_Bridge()
    {

    }

    internal static void Initialize()
    {
        if (KakaoManager.IsDebug)
        {
            Debug.Log(KakaoConstants.TAG + ": Instantiate::Start");
        }

        admobPluginClass = new AndroidJavaClass("com.microeyes.kakao.KakaoManager");
        unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        currActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        Send("Initialize", currActivity, true);

        if (KakaoManager.IsDebug)
        {
            Debug.Log(KakaoConstants.TAG + "Instantiate:: Finished");
        }
        
    }



    internal static void SendText(string a_text)
    {
        if (KakaoManager.IsDebug)
        {
            Debug.Log(KakaoConstants.TAG + "::SendText:: Start");
        }

        Send("SendText", a_text);

        if (KakaoManager.IsDebug)
        {
            Debug.Log(KakaoConstants.TAG + "::SendText:: Finished");
        }
    }

    internal static void SendAppLink(string a_linkText)
    {

        if (KakaoManager.IsDebug)
        {
            Debug.Log(KakaoConstants.TAG + "::SendAppLink:: Start");
        }

        Send("SendAppLink", a_linkText);

        if (KakaoManager.IsDebug)
        {
            Debug.Log(KakaoConstants.TAG + "::SendAppLink:: Finished");
        }
    }

    internal static void SendWebLink(string a_linkText, string a_webLink)
    {

        if (KakaoManager.IsDebug)
        {
            Debug.Log(KakaoConstants.TAG + "::SendWebLink:: Start");
        }

        Send("SendWebLink", a_linkText, a_webLink);

        if (KakaoManager.IsDebug)
        {
            Debug.Log(KakaoConstants.TAG + "::SendWebLink:: Finished");
        }
    }

    private static void Send(string a_functionName, params object[] a_data)
    {
        admobPluginClass.CallStatic
            (
                a_functionName,
                a_data
            );
    }
}
