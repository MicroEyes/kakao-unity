package com.microeyes.kakao;

import com.kakao.AppActionBuilder;
import com.kakao.AppActionInfoBuilder;
import com.kakao.KakaoLink;
import com.kakao.KakaoParameterException;
import com.kakao.KakaoTalkLinkMessageBuilder;

import android.app.Activity;
import android.util.Log;

public class KakaoManager {

	public static final String APP_TAG = "Kakao_Wrapper";
	
	static Activity MainActivity;
	
	private static KakaoLink kakaoLink;
    private static KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder;
	
    static KakaoManager s_instance = null;
    static KakaoManager getInstance() {
		if(s_instance == null)
		{
			Log.i(APP_TAG, "Initialize:: Creating instance");
			s_instance = new KakaoManager();
		}
		
		return s_instance;
    }
    
    static boolean IsDebug = false;
    
	public static void Initialize(final Activity a_currentActivity, final boolean a_IsDebug) {
		if(a_IsDebug) {
			Log.i(APP_TAG, "KakaoManager::Initialize:: Start");
		}
		MainActivity = a_currentActivity;
		IsDebug = a_IsDebug;
		getInstance().Internal_Initialize();
		
		if(a_IsDebug) {
			Log.i(APP_TAG, "KakaoManager::Initialize:: End");
		}
	}
	
	public static void SendText(String a_textType)
	{
		if(IsDebug) {
			Log.i(APP_TAG, "KakaoManager::SendText:: Message: '" + a_textType + "'");
		}
		getInstance().Internal_SendKakaoTalkLink(a_textType);
		kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
		
		if(IsDebug) {
			Log.i(APP_TAG, "KakaoManager::SendText:: END");
		}
	}
	
	public static void SendAppLink(String a_linkText) {
		if(IsDebug) {
			Log.i(APP_TAG, "KakaoManager::SendAppLink:: START");
		}
		
		getInstance().Internal_SendAppLink(a_linkText);
		kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
		
		if(IsDebug) {
			Log.i(APP_TAG, "KakaoManager::SendAppLink:: END");
		}
	}

	public static void SendWebLink(String a_linkText, String a_webLink) {
		if(IsDebug) {
			Log.i(APP_TAG, "KakaoManager::SendWebLink:: START");
		}
		
		getInstance().Internal_SendWebLink(a_linkText, a_webLink);
		kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
		
		if(IsDebug) {
			Log.i(APP_TAG, "KakaoManager::SendWebLink:: END");
		}
	}
	
	public void Internal_Initialize() {	
		try {
            kakaoLink = KakaoLink.getKakaoLink(MainActivity.getApplicationContext());
            kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
        } catch (KakaoParameterException e) {
        	Log.e(APP_TAG, "KakaoManager:: Initialize:: Error: " + e.getMessage());
        	e.printStackTrace();
        }
	}
	
	private void Internal_SendKakaoTalkLink(String textType) {
        try {
        	kakaoTalkLinkMessageBuilder.addText(textType);
        	kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder.build(), MainActivity);
	        } catch (KakaoParameterException e) {
	        	Log.e(APP_TAG, "KakaoManager:: Internal_SendKakaoTalkLink:: Error: " + e.getMessage());
	        	e.printStackTrace();
        }
    }
	
	private void Internal_SendAppLink(String a_linkText) {
		try {
			kakaoTalkLinkMessageBuilder.addAppLink(a_linkText,
			        new AppActionBuilder()
			            .addActionInfo(AppActionInfoBuilder.createAndroidActionInfoBuilder().setExecuteParam("execparamkey1=1111").setMarketParam("referrer=kakaotalklink").build())
			            .addActionInfo(AppActionInfoBuilder.createiOSActionInfoBuilder(AppActionBuilder.DEVICE_TYPE.PHONE).setExecuteParam("execparamkey1=1111").build()).build()
			    );
			
			kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder.build(), MainActivity);
		} catch (KakaoParameterException e) {
			// TODO Auto-generated catch block
			Log.e(APP_TAG, "KakaoManager:: Internal_SendAppLink:: Error: " + e.getMessage()); 
			e.printStackTrace();
		}
	}
	
	private void Internal_SendWebLink(String a_linkText, String a_link) {
		try {
			kakaoTalkLinkMessageBuilder.addWebLink(a_linkText, a_link);
			kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder.build(), MainActivity);
		} catch (KakaoParameterException e) {
			// TODO Auto-generated catch block
			Log.e(APP_TAG, "KakaoManager:: Internal_SendWebLink:: Error: " + e.getMessage()); 
			e.printStackTrace();
		}
	}
	
}
